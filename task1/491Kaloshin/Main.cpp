#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>
#include <algorithm>

class MySurface : public Application
{
public:
    MeshPtr _surface;

    ShaderProgramPtr _shader;

    float _detalization = 100.0;
    float _aa = 2;

private:

    glm::vec3 getCoords( const double u, const double v ) {
        double X = (_aa + cos(v / 2) * sin(u) - sin(v / 2) * sin(2 * u)) * cos(v);
        double Y = (_aa + cos(v / 2) * sin(u) - sin(v / 2) * sin(2 * u)) * sin(v);
        double Z = sin(v / 2) * sin(u) + cos(v / 2) * sin(2 * u);
        return glm::vec3(X, Y, Z);
    }

    glm::vec3 getNormals( const double u, const double v) {

        double xu = cos(v)*(cos(u)*cos(v/2) - 2*cos(2*u)*sin(v/2));
        double xv = 1./2*(2*sin(2*u)*sin(v/2)*sin(v) + sin(2*u)*(-cos(v/2))*cos(v) - sin(u)*sin(v/2)*cos(v) - 2*sin(u)*sin(v)*cos(v/2) - 2*_aa*sin(v));
        double yu = (cos(u)*cos(v/2) - 2*cos(2*u)*sin(v/2))*sin(v);
        double yv = 1./2*(-sin(u)*sin(v/2)*sin(v) + 2*sin(u)*cos(v/2)*cos(v) - 2*sin(2*u)*sin(v/2)*cos(v) - sin(2*u)*sin(v)*cos(v/2) + 2*_aa*cos(v));
        double zu = 2*cos(2*u)*cos(v/2) + cos(u)*sin(v/2);
        double zv = 1./2*cos(v/2)*sin(u) - 1./2*sin(2*u)*sin(v/2);

        double n_x = (yu*zv - yv*zu);
        double n_y = (-xu*zv + xv*zu);
        double n_z = (xu*yv - xv*yu);

        return glm::normalize( glm::vec3( n_x, n_y, n_z ) );
    }

public:
    MeshPtr buildSurface(unsigned int M) {
        std::vector<glm::vec3> points;
        std::vector<glm::vec3> normals;

        for( unsigned int i = 0; i < M; ++i ) {
            double u1 = 2. * (double)glm::pi<float>() * i / M;
            double u2 = 2. * (double)glm::pi<float>() * (i + 1) / M;

            for( unsigned int j = int(M/6); j < M; ++j ) {
                double v1 = 2. * (double)glm::pi<float>() * j / M;
                double v2 = 2. * (double)glm::pi<float>() * (j + 1) / M;

                //Первый треугольник

                points.push_back( getCoords( u1, v1 ) );
                points.push_back( getCoords( u1, v2 ) );
                points.push_back( getCoords( u2, v1 ) );

                normals.push_back( getNormals( u1, v1 ) );
                normals.push_back( getNormals( u1, v2 ) );
                normals.push_back( getNormals( u2, v1 ) );

                //Второй треугольник

                points.push_back( getCoords( u2, v2 ) );
                points.push_back( getCoords( u1, v2 ) );
                points.push_back( getCoords( u2, v1 ) );

                normals.push_back( getNormals( u2, v2 ) );
                normals.push_back( getNormals( u1, v2 ) );
                normals.push_back( getNormals( u2, v1 ) );
            }
        }

        DataBufferPtr vbuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        vbuf->setData(points.size() * sizeof(float) * 3, points.data());

        DataBufferPtr nbuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        nbuf->setData(normals.size() * sizeof(float) * 3, normals.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, vbuf);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, nbuf);

        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(points.size());

        return mesh;
    }

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        _surface = buildSurface(static_cast<unsigned int>(_detalization));

        _shader = std::make_shared<ShaderProgram>("491KaloshinData/shader.vert", "491KaloshinData/shader.frag");
    }

    void update() override
    {
        double dt = glfwGetTime() - _oldTime;

        if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS) {
            _detalization -= 1000.0f * dt;
            _detalization = std::max(4.0f, _detalization);
            std::cout << _detalization << std::endl;
         _surface = buildSurface(0.5f, static_cast<unsigned int>(_detalization));
        }
        if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
            _detalization += 1000.0f * dt;
            _detalization = std::min(100.0f, _detalization);
            std::cout << _detalization << std::endl;
         _surface = buildSurface(0.5f, static_cast<unsigned int>(_detalization));
        }
        Application::update();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
     _surface->draw();
    }
};

int main()
{
    MySurface app;
    app.start();

    return 0;
}